# fddd@mail.ru
# 123456
# fovoko2095@twit-mail.com

from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [

    path('', views.index, name='index'),
    path('drpz/', views.drpz, name='drpz'),
    path('drpz-salon/', views.drpz_salon, name='drpz_salon'),
    path('intim-salons', views.intim_salons, name='intim_salons'),
    path('f/', views.filtergirls, name='filtergirls'),
    path('test/', views.test, name='test'),
    path('signup/', views.signup, name='signup'),
    path('signin/', views.signin, name='signin'),
    path('account/', views.account_g, name='account_g'),
    path('replenishment/', views.replenishment, name='replenishment'),
    path('account/girls/add/', views.account_g_ad, name='account_g_ad'),
    path('account/intim-salons/add/', views.intim_salons_add, name='account_intim_salons_ad'),
    path('account/support/', views.support, name='support'),
    path('account/balance/history/', views.balance_history, name='balance_history'),
    path('account/personal/', views.personal, name='personal'),
    path('account/edit_g/<int:anketa_id>/', views.edit_g, name='edit_g'),
    path('account/edit_g/delete_photo/<int:anketa_id>/<int:photo>/', views.delete_photo, name='delete_photo'),
    path('account/make_normal/<int:anketa_id>/', views.make_normal, name='make_normal'),
    path('account/make_vip/<int:anketa_id>/', views.make_vip, name='make_vip'),
    path('account/make_inactive/<int:anketa_id>/', views.make_inactive, name='make_inactive'),
    path('logout_view/', views.logout_view, name='logout_view'),

    path('m/<str:metro>/', views.metro_choice, name='metro_choice'),
    path('m/<str:metro>/fm/', views.metro_choice_filter, name='metro_choice_filter'),
    path('individualka/<int:anketa_id>/', views.detail, name='detail'),
    path('individualka/<int:anketa_id>/leave_comment/', views.leave_comment, name='leave_comment'),

    path('intim-salons/<int:anketa_id>/', views.detail_intim_salons, name='detail_intim_salons'),
    path('intim-salons/<int:anketa_id>/leave_comment/', views.leave_comment_intim_salons, name='leave_comment_intim_salons'),
    path('account/edit_salon/<int:anketa_id>/', views.edit_salon, name='edit_salon'),
    path('account/edit_salon/delete_photo/<int:anketa_id>/<int:photo>/', views.delete_salon_photo,name='delete_salon_photo'),
    path('account/make_salon_normal/<int:anketa_id>/', views.make_salon_normal, name='make_salon_normal'),
    path('account/make_salon_inactive/<int:anketa_id>/', views.make_salon_inactive, name='make_salon_inactive'),


]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)