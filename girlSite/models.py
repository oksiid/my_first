from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
import django_filters
from django_filters import RangeFilter, CharFilter
from django.utils import timezone
from random import randrange

class balance(models.Model):

    id_user = models.ForeignKey(User, on_delete=models.CASCADE,)
    money = models.IntegerField(default=0)

class balance_history_plus(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE,)
    date = models.DateTimeField(auto_now_add=False, auto_now=False, default=timezone.now)
    money_plus = models.IntegerField()

class balance_history_minus(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE,)
    date = models.DateTimeField(auto_now_add=False, auto_now=False, default=timezone.now)
    money_minus = models.IntegerField()


@receiver(post_save, sender=balance_history_plus)
def balance_change_plus(sender, instance, **kwargs):
    b = balance.objects.filter(id_user=instance.id_user)
    for m in b:
        m.money += instance.money_plus
        m.save()

@receiver(post_save, sender=balance_history_minus)
def balance_change_minus(sender, instance, **kwargs):
    b = balance.objects.filter(id_user=instance.id_user)
    for m in b:
        m.money -= instance.money_minus
        m.save()

class country_available(models.Model):
    country = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.country

class city_available(models.Model):
    country_city = models.ForeignKey(country_available, on_delete=models.CASCADE,)
    city = models.CharField(max_length=100, blank=True)
    city_link = models.CharField(max_length=200, blank=True)
    def __str__(self):
        return self.city + '  '+ self.city_link

class metro_available(models.Model):
    city_metro = models.ForeignKey(city_available, on_delete=models.CASCADE,)
    metro = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.city_metro.city + '   ' + self.metro

class area_available(models.Model):
    city_area = models.ForeignKey(city_available, on_delete=models.CASCADE)
    area = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.city_area.city + '   '+ self.area

class individualka(models.Model):

    hair_color_choices = [
        ('не указано', 'не указано'),
        ('Брюнетка', 'Брюнетка'),
        ('Шатенка', 'Шатенка'),
        ('Рыжая', 'Рыжая'),
        ('Блондинка', 'Блондинка'),
        ('Русая', 'Русая'),
    ]

    intimate_haircut_choices = [
        ('не указано', 'не указано'),
        ('Полная депиляция', 'Полная депиляция'),
        ('Аккуратная стрижка', 'Аккуратная стрижка'),
        ('Натуральная', 'Натуральная'),
    ]

    nationality_choices = [
        ('не указано', 'не указано'),
        ('Абхазки', 'Абхазки'),
        ('Аварки', 'Аварки'),
        ('Адыгейки', 'Адыгейки'),
        ('Азербайджанки', 'Азербайджанки'),
        ('Азиатки', 'Азиатки'),
        ('Албанки', 'Албанки'),
        ('Алтайки', 'Алтайки'),
        ('Армянки', 'Армянки'),
        ('Балкарки', 'Балкарки'),
        ('Башкирки', 'Башкирки'),
        ('Белоруски', 'Белоруски'),
        ('Бурятки', 'Бурятки'),
        ('Вьетнамки', 'Вьетнамки'),
        ('Гагаузки', 'Гагаузки'),
        ('Грузинки', 'Грузинки'),
        ('Еврейки', 'Еврейки'),
        ('Ингушки', 'Ингушки'),
        ('Кабардинки', 'Кабардинки'),
        ('Казашки', 'Казашки'),
        ('Калмычки', 'Калмычки'),
        ('Каракалпачки', 'Каракалпачки'),
        ('Карачаевки', 'Карачаевки'),
        ('Карелки', 'Карелки'),
        ('Киргизки', 'Киргизки'),
        ('Китаянки', 'Китаянки'),
        ('Коми - пермячки', 'Коми - пермячки'),
        ('Комячки', 'Комячки'),
        ('Кореянки', 'Кореянки'),
        ('Крымчанки', 'Крымчанки'),
        ('Марийки', 'Марийки'),
        ('Монголки', 'Монголки'),
        ('Мордвинки', 'Мордвинки'),
        ('Мулатки', 'Мулатки'),
        ('Негритянки', 'Негритянки'),
        ('Ногайки', 'Ногайки'),
        ('Осетинки', 'Осетинки'),
        ('Русские', 'Русские'),
        ('Таджички', 'Таджички'),
        ('Татарки', 'Татарки'),
        ('Тайки', 'Тайки'),
        ('Тувинки', 'Тувинки'),
        ('Удмуртки', 'Удмуртки'),
        ('Узбечки', 'Узбечки'),
        ('Уйгурки', 'Уйгурки'),
        ('Украинки', 'Украинки'),
        ('Хакаски', 'Хакаски'),
        ('Ханты', 'Ханты'),
        ('Цыганки', 'Цыганки'),
        ('Черкески', 'Черкески'),
        ('Чеченки', 'Чеченки'),
        ('Чувашки', 'Чувашки'),
        ('Чукчи', 'Чукчи'),
        ('Эвенкийки', 'Эвенкийки'),
        ('Якутки', 'Якутки'),
        ('Японки', 'Японки'),
        ('Немки', 'Немки'),
        ('Кавказки', 'Кавказки'),
        ('Индианки', 'Индианки'),

    ]
    anketa = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    active_anketa = models.BooleanField(default=False, verbose_name='Активная анкета')
    VIP = models.BooleanField(default=False, blank=True)
    photo_approved = models.BooleanField(default=False, blank=True, verbose_name='Фото проверенно')
    comment_have = models.BooleanField(default=False, verbose_name='Есть отзывы')
    moderation = models.BooleanField(default=False, verbose_name='Прошло модерацию')
    number_of_views = models.IntegerField(default=0, verbose_name='Колличество просмотров')
    name = models.CharField(max_length=50, verbose_name='Имя')
    phone_number = models.CharField(max_length=20, verbose_name='Телефон')
    city = models.CharField(max_length=50, verbose_name='Город')# ВЫбор
    metro = models.CharField(max_length=50,verbose_name='Метро') #Выбор
    area = models.CharField(max_length=50,verbose_name='Район')  #Выбор
    girl_age = models.IntegerField( verbose_name='Возраст')# Новая форма в чарфилд
    girl_weight = models.IntegerField( verbose_name='Вес')# Новая форма в чарфилд
    girl_height = models.IntegerField( verbose_name='Рост')# Новая форма в чарфилд
    girl_chest = models.IntegerField( verbose_name='Грудь')# Новая форма в чарфилд
    exit_apartment = models.BooleanField(default=False, verbose_name='Выезд аппартаменты')
    exit_sauna = models.BooleanField(default=False, verbose_name='Выезд сауна')
    exit_hotel = models.BooleanField(default=False, verbose_name='Выезд отель')
    exit_office = models.BooleanField(default=False, verbose_name='Выезд офис')
    her_apartment = models.BooleanField(default=False, verbose_name='Личные аппартаменты')
    hair_color = models.CharField(max_length=20, choices=hair_color_choices, verbose_name='Цвет волос')
    intimate_haircut = models.CharField(max_length=20, choices=intimate_haircut_choices, verbose_name='Интимная стрижка')
    nationality = models.CharField(max_length=20, choices=nationality_choices, verbose_name='Национальность')
    tariff_apartment_1 = models.IntegerField(blank=True, default=0, verbose_name='Аппартаменты 1 час')
    tariff_apartment_2 = models.IntegerField(blank=True, default=0, verbose_name='Аппартаменты 2 часа')
    tariff_apartment_night = models.IntegerField(blank=True, default=0, verbose_name='Аппартаменты ночь')
    tariff_exit_1 = models.IntegerField(blank=True, default=0, verbose_name='Выезд 1 час')
    tariff_exit_2 = models.IntegerField(blank=True, default=0, verbose_name='Выезд 2 часа')
    tariff_exit_night = models.IntegerField(blank=True, default=0, verbose_name='Выезд Ночь')
    tariff_anal = models.IntegerField(blank=True, default=0, verbose_name='Цена анал')
    essay = models.TextField(verbose_name='Текст о себе', blank=True)
    usluga_type_1 = models.BooleanField(blank=True, default=False, verbose_name='Классический')
    usluga_type_2 = models.BooleanField(blank=True, default=False, verbose_name='Анальный')
    usluga_type_3 = models.BooleanField(blank=True, default=False, verbose_name='Групповой')
    usluga_type_4 = models.BooleanField(blank=True, default=False, verbose_name='Лесбийский')
    usluga_laska_1 = models.BooleanField(blank=True, default=False, verbose_name='С резинкой')
    usluga_laska_2 = models.BooleanField(blank=True, default=False, verbose_name='Без резинки')
    usluga_laska_3 = models.BooleanField(blank=True, default=False, verbose_name='Глубокий')
    usluga_laska_4 = models.BooleanField(blank=True, default=False, verbose_name='В машине')
    usluga_laska_5 = models.BooleanField(blank=True, default=False, verbose_name='Кунилингус')
    usluga_laska_6 = models.BooleanField(blank=True, default=False, verbose_name='Анилингус')
    usluga_sado_mazo_1 = models.BooleanField(blank=True, default=False, verbose_name='Бандаж')
    usluga_sado_mazo_2 = models.BooleanField(blank=True, default=False, verbose_name='Госпожа')
    usluga_sado_mazo_3 = models.BooleanField(blank=True, default=False, verbose_name='Ролевые игры')
    usluga_sado_mazo_4 = models.BooleanField(blank=True, default=False, verbose_name='Лёгкая доминация')
    usluga_sado_mazo_5 = models.BooleanField(blank=True, default=False, verbose_name='Порка')
    usluga_sado_mazo_6 = models.BooleanField(blank=True, default=False, verbose_name='Рабыня')
    usluga_sado_mazo_7 = models.BooleanField(blank=True, default=False, verbose_name='Фетиш')
    usluga_sado_mazo_8 = models.BooleanField(blank=True, default=False, verbose_name='Трамплинг')
    usluga_fisting_1 = models.BooleanField(blank=True, default=False, verbose_name='Анальный')
    usluga_fisting_2 = models.BooleanField(blank=True, default=False, verbose_name='Классический')
    usluga_zolotoy_1 = models.BooleanField(blank=True, default=False, verbose_name='Выдача')
    usluga_zolotoy_2 = models.BooleanField(blank=True, default=False, verbose_name='Приём')
    usluga_dopoln_1 = models.BooleanField(blank=True, default=False, verbose_name='Эскорт')
    usluga_dopoln_2 = models.BooleanField(blank=True, default=False, verbose_name='Фото/видео')
    usluga_dopoln_3 = models.BooleanField(blank=True, default=False, verbose_name='Услуги семейной паре')
    usluga_finish_1 = models.BooleanField(blank=True, default=False, verbose_name='В рот')
    usluga_finish_2 = models.BooleanField(blank=True, default=False, verbose_name='На лицо')
    usluga_finish_3 = models.BooleanField(blank=True, default=False, verbose_name='На грудь')
    usluga_massage_1 = models.BooleanField(blank=True, default=False, verbose_name='Классический')
    usluga_massage_2 = models.BooleanField(blank=True, default=False, verbose_name='Профессиональный')
    usluga_massage_3 = models.BooleanField(blank=True, default=False, verbose_name='Расслабляющий')
    usluga_massage_4 = models.BooleanField(blank=True, default=False, verbose_name='Тайский')
    usluga_massage_5 = models.BooleanField(blank=True, default=False, verbose_name='Урологический')
    usluga_massage_6 = models.BooleanField(blank=True, default=False, verbose_name='Точечный')
    usluga_massage_7 = models.BooleanField(blank=True, default=False, verbose_name='Эротический')
    usluga_massage_8 = models.BooleanField(blank=True, default=False, verbose_name='Ветка сакуры')
    usluga_strip_1 = models.BooleanField(blank=True, default=False, verbose_name='Профи')
    usluga_strip_2 = models.BooleanField(blank=True, default=False, verbose_name='Не профи')
    usluga_lesbi_1 = models.BooleanField(blank=True, default=False, verbose_name='Откровенное')
    usluga_lesbi_2 = models.BooleanField(blank=True, default=False, verbose_name='Лёгкое')
    usluga_extrim_1 = models.BooleanField(blank=True, default=False, verbose_name='Страпон')
    usluga_extrim_2 = models.BooleanField(blank=True, default=False, verbose_name='Игрушки')
    usluga_copro_1 = models.BooleanField(blank=True, default=False, verbose_name='Выдача')
    usluga_copro_2 = models.BooleanField(blank=True, default=False, verbose_name='Приём')


class individualka_photo(models.Model):
    photo_user = models.ForeignKey(individualka, on_delete=models.CASCADE, blank=True)
    photo = models.ImageField(upload_to='photos/', blank=True)

@receiver(post_delete, sender=individualka_photo)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)

def drpz_path(instance, filename):
    return 'dropzone_photos/{0}/{1}/{2}'.format(instance.photo_user, instance.date, filename)


class drpz_individualka(models.Model):
    photo_user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    time_to_delete = models.DateTimeField(auto_now_add=False, auto_now=False, default=timezone.now)
    date = models.CharField(max_length=50)
    photo = models.ImageField(upload_to=drpz_path, blank=True)

@receiver(post_delete, sender=drpz_individualka)
def submission_delete_drpz_photo(sender, instance, **kwargs):
    instance.photo.delete(False)

class comment(models.Model):
    anketa_post = models.ForeignKey(individualka, on_delete=models.CASCADE, blank=True)
    date = models.DateTimeField(auto_now_add=False, auto_now=False, default=timezone.now)
    author = models.CharField(max_length=50)
    text = models.TextField()
    photo_true = models.BooleanField(default=False)
    individualka_true = models.BooleanField(default=False)
    cl_sex_rating = models.IntegerField(default=0)
    anal_rating = models.IntegerField(default=0)
    minet_rating = models.IntegerField(default=0)
    service_rating = models.IntegerField(default=0)
    end_rating = models.IntegerField(default=0)

class Girls_Filter(django_filters.FilterSet):
    girl_age = RangeFilter()
    girl_weight = RangeFilter()
    girl_height = RangeFilter()
    girl_chest = RangeFilter()
    tariff_apartment_1 = RangeFilter()
    tariff_apartment_2 = RangeFilter()
    tariff_apartment_night = RangeFilter()
    phone_number = CharFilter()

    class Meta:
        model = individualka
        fields = ['girl_age', 'girl_weight', 'girl_height', 'girl_chest', 'tariff_apartment_1', 'tariff_apartment_2', 'tariff_apartment_night', 'phone_number',
                  'usluga_type_1', 'usluga_type_2', 'usluga_type_3', 'usluga_type_4', 'usluga_laska_1', 'usluga_laska_2', 'usluga_laska_3'
                  , 'usluga_laska_4', 'usluga_laska_5', 'usluga_laska_6', 'exit_apartment', 'exit_sauna', 'exit_hotel', 'exit_office'
                  , 'her_apartment', 'usluga_finish_1', 'usluga_finish_2', 'usluga_finish_3', 'photo_approved', 'comment_have',
                  ]


class intim_salon(models.Model):
    anketa = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    active_anketa = models.BooleanField(default=False, verbose_name='Активная анкета')
    comment_have = models.BooleanField(default=False, verbose_name='Есть отзывы')
    moderation = models.BooleanField(default=False, verbose_name='Прошло модерацию')
    number_of_views = models.IntegerField(default=0, verbose_name='Колличество просмотров')
    name = models.CharField(max_length=50, verbose_name='Имя')
    phone_number_1 = models.CharField(max_length=20, verbose_name='Телефон_1')
    phone_number_2 = models.CharField(max_length=20, verbose_name='Телефон_2', blank=True)
    phone_number_3 = models.CharField(max_length=20, verbose_name='Телефон_3', blank=True)
    work24 = models.BooleanField(default=False ,verbose_name='Вемя работы круголосуточно')
    work_time_start = models.CharField(max_length=20, verbose_name='Время работы С', blank=True)
    work_time_close = models.CharField(max_length=20, verbose_name='Время работы ДО', blank=True)
    city = models.CharField(max_length=50, verbose_name='Город')  # ВЫбор
    metro = models.CharField(max_length=50, verbose_name='Метро')  # Выбор
    area = models.CharField(max_length=50, verbose_name='Район')  # Выбор
    address = models.CharField(max_length=50, verbose_name='Адрес', blank=True)  # Выбор
    tariff_1 = models.IntegerField(blank=True, default=0, verbose_name='Тариф 1 час')
    tariff_2 = models.IntegerField(blank=True, default=0, verbose_name='Тариф 2 часа')
    tariff_night = models.IntegerField(blank=True, default=0, verbose_name='Тариф ночь')
    tariff_anal = models.IntegerField(blank=True, default=0, verbose_name='Цена анал')
    essay = models.TextField(verbose_name='Текст о себе', blank=True)

class intim_salon_photo(models.Model):
    photo_user = models.ForeignKey(intim_salon, on_delete=models.CASCADE, blank=True)
    photo = models.ImageField(upload_to='photos_intim_salon/', blank=True)

@receiver(post_delete, sender=intim_salon)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)

class drpz_intim_salon(models.Model):
    photo_user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    time_to_delete = models.DateTimeField(auto_now_add=False, auto_now=False, default=timezone.now)
    date = models.CharField(max_length=50)
    photo = models.ImageField(upload_to=drpz_path, blank=True)

@receiver(post_delete, sender=drpz_intim_salon)
def submission_delete_drpz__salon_photo(sender, instance, **kwargs):
    instance.photo.delete(False)

class comment_intim_salon(models.Model):
    anketa_post = models.ForeignKey(intim_salon, on_delete=models.CASCADE, blank=True)
    date = models.DateTimeField(auto_now_add=False, auto_now=False, default=timezone.now)
    author = models.CharField(max_length=50)
    text = models.TextField()
    cl_sex_rating = models.IntegerField(default=0)
    anal_rating = models.IntegerField(default=0)
    minet_rating = models.IntegerField(default=0)
    service_rating = models.IntegerField(default=0)
    end_rating = models.IntegerField(default=0)

class test(models.Model):
    test_user = models.ForeignKey(User,on_delete=models.CASCADE, default=1)
    text = models.CharField(max_length=50)
    chek = models.BooleanField(default=False)

class test_photos(models.Model):
    photo_test_user = models.ForeignKey(test, on_delete=models.CASCADE, blank=True)
    photo = models.ImageField(upload_to='images/', blank=True)

