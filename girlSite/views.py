from django.http import Http404, HttpResponseRedirect, QueryDict
import requests
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, reverse
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.files.base import ContentFile
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.utils import timezone
from girlSite.tasks import views_plus
from celery import current_app
from datetime import timedelta
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.core.cache import cache
import random
from bs4 import BeautifulSoup
from django.core.files import File


RANDOM_EXPERIENCES=20

def drpz(request):
    if request.user.is_authenticated:

        if request.method == "POST":
            for f in request.FILES.getlist('file'):
                data = f.read()
                photo = drpz_individualka()
                photo.photo_user = request.user
                photo.date = request.POST['date']
                photo.photo.save(f.name, ContentFile(data), )
                photo.save()

            return redirect('account_g')


    else:
        return redirect('signin')

def drpz_salon(request):
    if request.user.is_authenticated:

        if request.method == "POST":
            for f in request.FILES.getlist('file'):
                data = f.read()
                photo = drpz_intim_salon()
                photo.photo_user = request.user
                photo.date = request.POST['date']
                photo.photo.save(f.name, ContentFile(data), )
                photo.save()

            return redirect('account_g')


    else:
        return redirect('signin')

def index(request):

    if not request.session.get('random_exp'):
        request.session['random_exp'] = random.randrange(0, RANDOM_EXPERIENCES)
    girls_all = cache.get('random_exp_%d' % request.session['random_exp'])
    if not girls_all:
        girls_all = list(individualka.objects.filter(active_anketa=True, moderation=True).order_by('-VIP','?'))
        cache.set('random_exp_%d' % request.session['random_exp'], girls_all, 60*10)


    paginator = Paginator(girls_all, 40)

    page_number = request.GET.get('page')
    girls = paginator.get_page(page_number)
    
    try:
        cr_p = int(page_number)
    except:
        cr_p = 1

    cr_p_plus1 = cr_p+1
    cr_p_plus2 = cr_p+2
    cr_p_plus3 = cr_p+3
    cr_p_plus4 = cr_p+4
    cr_p_minus1 = cr_p-1
    cr_p_minus2 = cr_p-2
    cr_p_minus3 = cr_p-3
    cr_p_minus4 = cr_p-4
    context = {
        'girls': girls,
        'country': country_available.objects.all(),
        'city': city_available.objects.all(),
        'metro': metro_available.objects.all(),
        'paginator': paginator,
        'cr_p': cr_p,
        'cr_plus1': cr_p_plus1,
        'cr_plus2': cr_p_plus2,
        'cr_plus3': cr_p_plus3,
        'cr_plus4': cr_p_plus4,
        'cr_minus1': cr_p_minus1,
        'cr_minus2': cr_p_minus2,
        'cr_minus3': cr_p_minus3,
        'cr_minus4': cr_p_minus4,
    }

    return render(request, 'index.html', context)


def intim_salons(request):
    if not request.session.get('random_exp_intim_salon'):
        request.session['random_exp_intim_salon'] = random.randrange(0, RANDOM_EXPERIENCES)
    intim_s = cache.get('random_exp_intim_salon_%d' % request.session['random_exp_intim_salon'])
    if not intim_s:
        intim_s = list(intim_salon.objects.filter(active_anketa=True, moderation=True).order_by('?'))
        cache.set('random_exp_intim_salon_%d' % request.session['random_exp_intim_salon'], intim_s, 10)

    paginator = Paginator(intim_s, 40)

    page_number = request.GET.get('page')
    intim_salons_on_page = paginator.get_page(page_number)

    try:
        cr_p = int(page_number)
    except:
        cr_p = 1

    cr_p_plus1 = cr_p + 1
    cr_p_plus2 = cr_p + 2
    cr_p_plus3 = cr_p + 3
    cr_p_plus4 = cr_p + 4
    cr_p_minus1 = cr_p - 1
    cr_p_minus2 = cr_p - 2
    cr_p_minus3 = cr_p - 3
    cr_p_minus4 = cr_p - 4
    context = {
        'girls': intim_salons_on_page,
        'country': country_available.objects.all(),
        'city': city_available.objects.all(),
        'metro': metro_available.objects.all(),
        'paginator': paginator,
        'cr_p': cr_p,
        'cr_plus1': cr_p_plus1,
        'cr_plus2': cr_p_plus2,
        'cr_plus3': cr_p_plus3,
        'cr_plus4': cr_p_plus4,
        'cr_minus1': cr_p_minus1,
        'cr_minus2': cr_p_minus2,
        'cr_minus3': cr_p_minus3,
        'cr_minus4': cr_p_minus4,
    }
    return render(request, 'intim_salons.html', context)

def detail_intim_salons(request, anketa_id):
    try:

        a = intim_salon.objects.get(id=anketa_id)

        comments_all = comment_intim_salon.objects.filter(anketa_post=anketa_id).order_by('-pk')
        # views_plus.delay(anketa_id)

        context = {
            'recaptcha': settings.GOOGLE_RECAPTCHA_PUB_KEY,
            'comments_all': comments_all,
            'girl': a,
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),
        }


    except:
        raise Http404('Статья не найдена')

    return render(request, 'detail_intim_salons.html', context)

def leave_comment_intim_salons(request, anketa_id):
    try:
        a = intim_salon.objects.get(id = anketa_id)
    except:
        raise Http404("PAge nor found")
    ''' Begin reCAPTCHA validation '''
    recaptcha_response = request.POST.get('g-recaptcha-response')
    data = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
    }
    r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
    result = r.json()
    ''' End reCAPTCHA validation '''
    if result['success']:

        a.comment_intim_salon_set.create(author=request.POST['author'], text=request.POST['text'],
                             cl_sex_rating=request.POST['r_1'], anal_rating=request.POST['r_2'],
                             minet_rating=request.POST['r_3'], service_rating=request.POST['r_4'],
                             end_rating=request.POST['r_5'],
                             )
        if a.comment_have == False:
            a.comment_have = True
            a.save()

        return HttpResponseRedirect(reverse('detail_intim_salons', args=(anketa_id,)))
    else:
        pass


    return HttpResponseRedirect(reverse('detail_intim_salons', args=(anketa_id,)))

def intim_salons_add(request):
    if request.user.is_authenticated:
        date = timezone.now()
        date = str(date.time())
        date = date.replace(':', '_')
        context = {
            'user': request.user,
            'intim_salon_form': intim_salon_form,
            'city_av': city_available.objects.all(),
            'area_av': area_available.objects.all(),
            'metro_av': metro_available.objects.all(),
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),
            'date': date,

        }

        if request.method == "POST":

            form = intim_salon_form(request.POST, request.FILES)
            if form.is_valid():
                new_form = form.save(commit=False)
                new_form.anketa = request.user
                new_form.city = request.POST['city']
                new_form.area = request.POST['area']
                new_form.metro = request.POST['metro']
                new_form.save()

                # for f in request.FILES.getlist('photos'):
                #     data = f.read()
                #     photo = individualka_photo()
                #     photo.photo_user = new_form
                #     photo.photo.save(f.name, ContentFile(data))
                #     photo.save()

                photos = drpz_intim_salon.objects.filter(photo_user=request.user, date=request.POST['date'])
                for p in photos:
                    photo = intim_salon_photo()
                    photo.photo_user = new_form
                    photo.photo.save(p.photo.name, File(open(str(p.photo.path), 'rb')))
                    photo.save()
                    p.delete()
                messages.info(request, 'Анкета отправлена на модерацию! Поверка длится до 24 часов')
                return redirect('account_g')
            else:
                print(form.errors)

        return render(request, 'account_salon_ad.html', context)
    else:
        return redirect('signin')

def edit_salon(request, anketa_id):
    if request.user.is_authenticated:
        try:
            a = intim_salon.objects.get(id=anketa_id, anketa=request.user)
        except:
            return redirect('account_g')

        context = {
            'user': request.user,
            'anketa_id': anketa_id,
            'photo': intim_salon_photo.objects.filter(photo_user=a),
            'intim_salon_form': intim_salon_form(instance=a),
            'city_av': city_available.objects.all(),
            'area_av': area_available.objects.all(),
            'metro_av': metro_available.objects.all(),
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),

        }

        if request.method == "POST":

            form = intim_salon_form(request.POST, request.FILES, instance=a)
            if form.is_valid():
                new_form = form.save(commit=False)
                new_form.moderation = False
                new_form.anketa = request.user
                new_form.city = request.POST['city']
                new_form.area = request.POST['area']
                new_form.metro = request.POST['metro']
                new_form.save()

                photos = drpz_intim_salon.objects.filter(photo_user=request.user, date=request.POST['date'])
                for p in photos:
                    photo = intim_salon_photo()
                    photo.photo_user = new_form
                    photo.photo.save(p.photo.name, File(open(str(p.photo.path), 'rb')))
                    photo.save()
                    p.delete()
                messages.info(request, 'Анкета отправлена на модерацию! Поверка длится до 24 часов')
                return redirect('account_g')
            else:
                print(form.errors)

        return render(request, 'edit_intim_salon.html', context)
    else:
        return redirect('signin')

def delete_salon_photo(request, anketa_id, photo):
    if request.user.is_authenticated:
        try:
            a = intim_salon.objects.get(id=anketa_id, anketa=request.user)
            p = intim_salon_photo.objects.get(id=photo)
            p.delete()
            return redirect('edit_salon', anketa_id)
        except:
            return redirect('account_g')
    else:
        return redirect('signin')

def make_salon_normal(request, anketa_id):
    if request.user.is_authenticated:
        try:
            a = intim_salon.objects.get(id=anketa_id, anketa=request.user)
            a.active_anketa = True
            a.save()
            return redirect('account_g')
        except:
            return redirect('account_g')
    else:
        return redirect('signin')

def make_salon_inactive(request, anketa_id):
    if request.user.is_authenticated:
        try:
            a = intim_salon.objects.get(id=anketa_id, anketa=request.user)
            a.active_anketa = False
            a.save()
            return redirect('account_g')
        except:
            return redirect('account_g')
    else:
        return redirect('signin')

def filtergirls(request):

    rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
    rq_get_1 = rq_get_mutable.urlencode()
    if not request.GET.get('tariff_apartment_1_max') == '' and not request.GET.get('tariff_apartment_2_max') == '' and not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_2_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_night_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_1_max') == '' and not request.GET.get('tariff_apartment_2_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_2_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_1_max') == '' and not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_night_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_2_max') == '' and not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_2_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_night_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_1_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min', 1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_2_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_2_min', 1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_night_min', 1)
        rq_get_1 = rq_get_mutable.urlencode()



    rq_get_2 = rq_get_1.split('&page=')
    rq_get = str(rq_get_2[0])
    girls_all = individualka.objects.filter(active_anketa=True, moderation=True).order_by('-VIP', '?')
    if not request.session.get('random_exp_f_'+rq_get):
        request.session['random_exp_f_'+rq_get] = rq_get
    f_list = cache.get('random_exp_f_'+rq_get)
    if not f_list:
        f = Girls_Filter(rq_get_mutable, queryset=girls_all)
        f_list = f.qs
        cache.set('random_exp_f_'+ rq_get, f_list, 60*10)

    paginator = Paginator(f_list, 2)

    page_number = request.GET.get('page')
    girls = paginator.get_page(page_number)
    try:
        cr_p = int(page_number)
    except:
        cr_p = 1

    cr_p_plus1 = cr_p+1
    cr_p_plus2 = cr_p+2
    cr_p_plus3 = cr_p+3
    cr_p_plus4 = cr_p+4
    cr_p_minus1 = cr_p-1
    cr_p_minus2 = cr_p-2
    cr_p_minus3 = cr_p-3
    cr_p_minus4 = cr_p-4
    context = {
        'girls':girls,
        'country': country_available.objects.all(),
        'city': city_available.objects.all(),
        'metro': metro_available.objects.all(),
        'rq_get':rq_get,
        'paginator':paginator,
        'cr_p': cr_p,
        'cr_plus1': cr_p_plus1,
        'cr_plus2': cr_p_plus2,
        'cr_plus3': cr_p_plus3,
        'cr_plus4': cr_p_plus4,
        'cr_minus1': cr_p_minus1,
        'cr_minus2': cr_p_minus2,
        'cr_minus3': cr_p_minus3,
        'cr_minus4': cr_p_minus4,
    }

    return render(request, 'filter.html', context)

def metro_choice(request, metro):

    if not request.session.get('random_exp_'+metro):
        request.session['random_exp_'+metro] = metro
    girls_all = cache.get('random_exp_' +metro)
    if not girls_all:
        girls_all = list(individualka.objects.filter(metro=metro, active_anketa=True, moderation=True).order_by('-VIP','?'))
        cache.set('random_exp_'+metro, girls_all, 60*10)


    paginator = Paginator(girls_all, 40)

    page_number = request.GET.get('page')
    girls = paginator.get_page(page_number)

    try:
        cr_p = int(page_number)
    except:
        cr_p = 1

    cr_p_plus1 = cr_p+1
    cr_p_plus2 = cr_p+2
    cr_p_plus3 = cr_p+3
    cr_p_plus4 = cr_p+4
    cr_p_minus1 = cr_p-1
    cr_p_minus2 = cr_p-2
    cr_p_minus3 = cr_p-3
    cr_p_minus4 = cr_p-4
    context = {
        'girls': girls,
        'country': country_available.objects.all(),
        'city': city_available.objects.all(),
        'metro': metro_available.objects.all(),
        'paginator': paginator,
        'cr_p': cr_p,
        'cr_plus1': cr_p_plus1,
        'cr_plus2': cr_p_plus2,
        'cr_plus3': cr_p_plus3,
        'cr_plus4': cr_p_plus4,
        'cr_minus1': cr_p_minus1,
        'cr_minus2': cr_p_minus2,
        'cr_minus3': cr_p_minus3,
        'cr_minus4': cr_p_minus4,
        'metro_current': metro,
    }

    return render(request, 'metro_choice.html', context)

def metro_choice_filter(request, metro):

    rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
    rq_get_1 = rq_get_mutable.urlencode()
    if not request.GET.get('tariff_apartment_1_max') == '' and not request.GET.get('tariff_apartment_2_max') == '' and not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_2_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_night_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_1_max') == '' and not request.GET.get('tariff_apartment_2_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_2_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_1_max') == '' and not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_night_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_2_max') == '' and not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_2_min',1)
        rq_get_mutable.__setitem__('tariff_apartment_night_min',1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_1_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_1_min', 1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_2_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_2_min', 1)
        rq_get_1 = rq_get_mutable.urlencode()
    elif not request.GET.get('tariff_apartment_night_max') == '':
        rq_get_mutable = QueryDict(request.GET.urlencode(), mutable=True)
        rq_get_mutable.__setitem__('tariff_apartment_night_min', 1)
        rq_get_1 = rq_get_mutable.urlencode()

    rq_get_2 = rq_get_1.split('&page=')
    rq_get = str(rq_get_2[0])
    girls_all = individualka.objects.filter(metro=metro, active_anketa=True, moderation=True).order_by('-VIP', '?')
    if not request.session.get('random_exp_f_'+metro + rq_get):
        request.session['random_exp_f_'+metro + rq_get] = metro+rq_get
    f_list = cache.get('random_exp_f_'+metro + rq_get)
    if not f_list:
        f = Girls_Filter(rq_get_mutable, queryset=girls_all)
        f_list = f.qs
        cache.set('random_exp_f_'+metro + rq_get, f_list, 60 * 10)

    paginator = Paginator(f_list, 40)

    page_number = request.GET.get('page')
    girls = paginator.get_page(page_number)
    try:
        cr_p = int(page_number)
    except:
        cr_p = 1

    cr_p_plus1 = cr_p+1
    cr_p_plus2 = cr_p+2
    cr_p_plus3 = cr_p+3
    cr_p_plus4 = cr_p+4
    cr_p_minus1 = cr_p-1
    cr_p_minus2 = cr_p-2
    cr_p_minus3 = cr_p-3
    cr_p_minus4 = cr_p-4
    context = {
        'girls': girls,
        'country': country_available.objects.all(),
        'city': city_available.objects.all(),
        'metro': metro_available.objects.all(),
        'rq_get': rq_get,
        'paginator': paginator,
        'cr_p': cr_p,
        'cr_plus1': cr_p_plus1,
        'cr_plus2': cr_p_plus2,
        'cr_plus3': cr_p_plus3,
        'cr_plus4': cr_p_plus4,
        'cr_minus1': cr_p_minus1,
        'cr_minus2': cr_p_minus2,
        'cr_minus3': cr_p_minus3,
        'cr_minus4': cr_p_minus4,
        'metro_current': metro,
    }

    return render(request, 'metro_choice_filter.html', context)

def detail(request, anketa_id):
    try:

        a = individualka.objects.get(id=anketa_id)
        girls = individualka.objects.filter(metro=a.metro, moderation=True, active_anketa=True).exclude(id=anketa_id)[:4]
        comments_all = comment.objects.filter(anketa_post=anketa_id).order_by('-pk')
        #views_plus.delay(anketa_id)
        next_girl = anketa_id +1
        try:
            next_girl = individualka.objects.get(id=next_girl)
        except:
            next_girl= None
        previous_girl = anketa_id -1
        try:
            previous_girl = individualka.objects.get(id = previous_girl)
        except:
            previous_girl = None
        context = {
            'recaptcha': settings.GOOGLE_RECAPTCHA_PUB_KEY,
            'comments_all':comments_all,
            'girls': girls,
            'girl': a,
            'ng':next_girl,
            'pg':previous_girl,
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),
        }


    except:
        raise Http404('Статья не найдена')

    return render(request, 'detail.html', context)

def leave_comment(request, anketa_id):
    try:
        a = individualka.objects.get(id = anketa_id)
    except:
        raise Http404("PAge nor found")
    ''' Begin reCAPTCHA validation '''
    recaptcha_response = request.POST.get('g-recaptcha-response')
    data = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
    }
    r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
    result = r.json()
    ''' End reCAPTCHA validation '''
    if result['success']:
        try:
            photo_true=request.POST['photo_true']

        except:
            photo_true = False


        try:
            individualka_true = request.POST['individualka_true']
        except:
            individualka_true = False


        a.comment_set.create(author=request.POST['author'], text=request.POST['text'],
                             cl_sex_rating=request.POST['r_1'], anal_rating=request.POST['r_2'],
                             minet_rating=request.POST['r_3'], service_rating=request.POST['r_4'],
                             end_rating=request.POST['r_5'], photo_true=photo_true, individualka_true=individualka_true,
                             )
        if a.comment_have == False:
            a.comment_have = True
            a.save()

        return HttpResponseRedirect(reverse('detail', args=(anketa_id,)))
    else:
        pass


    return HttpResponseRedirect(reverse('detail', args=(anketa_id,)))


def signup(request):
    if request.user.is_authenticated:
        return redirect('account_g')
    else: pass
    if request.method == "POST":
        email = request.POST['email']
        password_1 = request.POST['password_1']
        password_2 = request.POST['password_2']
        if password_1 == password_2:
            try:
                user = User.objects.create_user(email, email, password_1)
                user.save()
                username = request.POST['email']
                password = request.POST['password_1']
                user = authenticate(request, username=username, password=password)

                balance.objects.create(id_user= user)
                if user is not None:
                    login(request, user)
                    return redirect('account_g')

                else:
                    return redirect('signup')

            except:
                messages.info(request, 'Пользователь с таким email уже зарегитсрирован')
                messages.info(request, 'Войти')
                return redirect('signup',)
        else:
            messages.info(request, 'Пароли не совпадают')
            return redirect('signup')

    return render(request, 'signUP.html', )


def signin(request):
    if request.user.is_authenticated:
        return redirect('account_g')
    else: pass
    if request.method == "POST":
        username = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('account_g')

        else:
            messages.info(request, 'Неверный пароль')
            return redirect('signin')

    else:
        pass

    return render(request, 'signIN.html')

def account_g(request):
    if request.user.is_authenticated:
        girls = individualka.objects.filter(anketa=request.user.id)
        girls_active = girls.filter(active_anketa=True, moderation=True)
        girls_no_active = girls.filter(active_anketa=False, moderation=True)
        girls_moderation = girls.filter(moderation=False)
        girls_VIP = girls.filter(active_anketa=True, moderation=True, VIP=True)
        girls_no_VIP = girls.filter(active_anketa=True, moderation=True, VIP=False)

        salons = intim_salon.objects.filter(anketa=request.user.id)
        salons_active = intim_salon.objects.filter(active_anketa=True, moderation=True)
        salons_no_active = intim_salon.objects.filter(active_anketa=False, moderation=True)
        salons_moderation = intim_salon.objects.filter(moderation=False)



        paymant_all_1 = 0
        paymant_all_7 = 0
        paymant_all_30 = 0
        payment_1 = balance_history_minus.objects.filter(id_user=request.user.id, date__gte=timezone.now()-timedelta(days=1))
        payment_7 = balance_history_minus.objects.filter(id_user=request.user.id, date__gte=timezone.now()-timedelta(days=7))
        payment_30 = balance_history_minus.objects.filter(id_user=request.user.id, date__gte=timezone.now()-timedelta(days=30))

        for p in payment_1:
            paymant_all_1 += p.money_minus

        for p in payment_7:
            paymant_all_7 += p.money_minus

        for p in payment_30:
            paymant_all_30 += p.money_minus


        context = {
            'user': request.user,
            'girls': girls,
            'girls_active': girls_active,
            'girls_no_active': girls_no_active,
            'girls_moderation': girls_moderation,
            'girls_VIP': girls_VIP,
            'girls_no_VIP': girls_no_VIP,

            'salons_active':salons_active,
            'salons_no_active':salons_no_active,
            'salons_moderation':salons_moderation,

            'paymant_all_1':paymant_all_1,
            'paymant_all_7':paymant_all_7,
            'paymant_all_30':paymant_all_30,
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),

        }

        return render(request, 'account_g.html', context)
    else:
        return redirect('signin')


def replenishment(request):
    context = {
        'country': country_available.objects.all(),
        'city': city_available.objects.all(),
        'metro': metro_available.objects.all(),

    }

    return render(request, 'replenishment.html', context)

def account_g_ad(request):
    if request.user.is_authenticated:
        date = timezone.now()
        date = str(date.time())
        date = date.replace(':','_')
        context = {
            'user': request.user,
            'individualka_form': individualka_form,
            'individualka_photo_form': individualka_photo_form,
            'city_av': city_available.objects.all(),
            'area_av': area_available.objects.all(),
            'metro_av': metro_available.objects.all(),
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),
            'date': date,

        }

        if request.method == "POST":

            form = individualka_form(request.POST, request.FILES)
            if form.is_valid():
                new_form = form.save(commit=False)
                new_form.anketa = request.user
                new_form.city = request.POST['city']
                new_form.area = request.POST['area']
                new_form.metro = request.POST['metro']
                new_form.save()

                # for f in request.FILES.getlist('photos'):
                #     data = f.read()
                #     photo = individualka_photo()
                #     photo.photo_user = new_form
                #     photo.photo.save(f.name, ContentFile(data))
                #     photo.save()

                photos = drpz_individualka.objects.filter(photo_user=request.user, date=request.POST['date'])
                for p in photos:
                    photo = individualka_photo()
                    photo.photo_user = new_form
                    photo.photo.save(p.photo.name, File(open(str(p.photo.path), 'rb')))
                    photo.save()
                    p.delete()
                messages.info(request, 'Анкета отправлена на модерацию! Поверка длится до 24 часов')
                return redirect('account_g')
            else:
                print(form.errors)


        return render(request, 'account_g_ad.html', context)
    else:
        return redirect('signin')

def edit_g(request, anketa_id):
    if request.user.is_authenticated:
        try:
            a = individualka.objects.get(id=anketa_id, anketa=request.user)
        except:
            return redirect('account_g')

        context = {
            'user': request.user,
            'anketa_id': anketa_id,
            'photo': individualka_photo.objects.filter(photo_user= a),
            'individualka_form': individualka_form(instance= a),
            'individualka_photo_form': individualka_photo_form(),
            'city_av': city_available.objects.all(),
            'area_av': area_available.objects.all(),
            'metro_av': metro_available.objects.all(),
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),

        }

        if request.method == "POST":

            form = individualka_form(request.POST, request.FILES, instance=a)
            if form.is_valid():
                new_form = form.save(commit=False)
                new_form.moderation = False
                new_form.anketa = request.user
                new_form.city = request.POST['city']
                new_form.area = request.POST['area']
                new_form.metro = request.POST['metro']
                new_form.save()

                # for f in request.FILES.getlist('photos'):
                #     data = f.read()
                #     photo = individualka_photo()
                #     photo.photo_user = new_form
                #     photo.photo.save(f.name, ContentFile(data))
                #     photo.save()
                photos = drpz_individualka.objects.filter(photo_user=request.user, date=request.POST['date'])
                for p in photos:
                    photo = individualka_photo()
                    photo.photo_user = new_form
                    photo.photo.save(p.photo.name, File(open(str(p.photo.path), 'rb')))
                    photo.save()
                    p.delete()
                messages.info(request, 'Анкета отправлена на модерацию! Поверка длится до 24 часов')
                return redirect('account_g')
            else:
                print(form.errors)

        return render(request, 'edit_g.html', context)
    else:
        return redirect('signin')

def delete_photo(request, anketa_id, photo):
    if request.user.is_authenticated:
        try:
            a = individualka.objects.get(id = anketa_id, anketa=request.user)
            p = individualka_photo.objects.get(id=photo)
            p.delete()
            return redirect('edit_g', anketa_id)
        except:
            return redirect('account_g')
    else:
        return redirect('signin')

def support(request):
    if request.user.is_authenticated:

        context = {
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),

        }

        if request.method == "POST":
            subject = request.POST['email'] + '  ' + request.POST['subject']
            send_mail(
                subject,
                request.POST['text'],
                'igor_kovalchuk_1976@mail.ru',
                ['igor_kovalchuk_1976@mail.ru'],
                fail_silently=False,
            )
            messages.info(request, 'Сообщение отправлено!')
            return redirect('support')

        return render(request, 'support.html', context)
    else:
        return redirect('signin')


def balance_history(request):
    if request.user.is_authenticated:

        context = {
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),
            'bm': balance_history_minus.objects.filter(id_user=request.user.id).order_by('-id'),
            'bp': balance_history_plus.objects.filter(id_user=request.user.id).order_by('-id')

        }


        return render(request, 'balance.html', context)
    else:
        return redirect('signin')


def personal(request):
    if request.user.is_authenticated:

        context = {
            'country': country_available.objects.all(),
            'city': city_available.objects.all(),
            'metro': metro_available.objects.all(),

        }

        if request.method == "POST":
            if request.POST['password_1'] == request.POST['password_2']:
                u = User.objects.get(username=request.user)
                u.set_password(request.POST['password_1'])
                u.save()
                user = authenticate(request, username=request.user, password=request.POST['password_1'])
                if user is not None:
                    login(request, user)
                    messages.info(request, 'Пароль изменен!')
                    return redirect('personal')

                return redirect('personal')
            else:
                messages.error(request, 'Пароли не совпадают!')
                return redirect('personal')

        return render(request, 'personal.html', context)
    else:
        return redirect('signin')


def make_normal(request, anketa_id):
    if request.user.is_authenticated:
        try:
            a = individualka.objects.get(id=anketa_id, anketa=request.user)
            a.VIP = False
            a.active_anketa = True
            a.save()
            return redirect('account_g')
        except:
            return redirect('account_g')
    else:
        return redirect('signin')


def make_vip(request, anketa_id):
    if request.user.is_authenticated:
        try:
            a = individualka.objects.get(id=anketa_id, anketa=request.user)
            a_vip_all = individualka.objects.filter(anketa=request.user, VIP=True).count()
            if a_vip_all == 1:
                a_vip_all = 10
            else:
                a_vip_all = a_vip_all * 10
                a_vip_all = a_vip_all + 10
            b = balance.objects.get(id_user=request.user)
            if b.money >= a_vip_all:
                a.VIP = True
                a.save()
            else:
                messages.info(request, 'Недостаточно средств на балансе!')
            return redirect('account_g')
        except:
            return redirect('account_g')
    else:
        return redirect('signin')

def make_inactive(request, anketa_id):
    if request.user.is_authenticated:
        try:
            a = individualka.objects.get(id=anketa_id, anketa=request.user)
            a.active_anketa = False
            a.save()
            return redirect('account_g')
        except:
            return redirect('account_g')
    else:
        return redirect('signin')

def logout_view(request):
    logout(request)
    return redirect('index')


@csrf_exempt
def test(request):
    context = {
        'form': individualka_form,
        'form_photo': individualka_photo_form
    }

    if request.method == "POST":

        form = individualka_form(request.POST, request.FILES)
        if form.is_valid():
            new_form = form.save(commit=False)

            new_form.save()

            for f in request.FILES.getlist('photos'):
                data = f.read()
                photo = individualka_photo()
                photo.photo_user = new_form
                photo.photo.save(f.name, ContentFile(data))
                photo.save()
            print(request.POST)
    return render(request, 'test.html', context)