from django.contrib import admin
from .models import *

class individualka_photoInline(admin.TabularInline):
    fk_name = 'photo_user'
    model = individualka_photo


class individualkaAdmin(admin.ModelAdmin):
    list_filter = ('moderation', 'active_anketa', 'VIP', 'photo_approved')
    list_display = ('anketa', 'name', 'phone_number', 'VIP')
    search_fields = ['anketa__email', 'name', 'phone_number']
    inlines = [individualka_photoInline]

####Интим салоны
class intim_salon_photoInline(admin.TabularInline):
    fk_name = 'photo_user'
    model = intim_salon_photo


class intim_salon_Admin(admin.ModelAdmin):
    list_filter = ('moderation', 'active_anketa')
    list_display = ('anketa', 'name', 'phone_number_1')
    search_fields = ['anketa__email', 'name', 'phone_number_1','phone_number_2', 'phone_number_3']
    inlines = [intim_salon_photoInline]


admin.site.register(individualka, individualkaAdmin)
admin.site.register(intim_salon, intim_salon_Admin)
admin.site.register(city_available)
admin.site.register(metro_available)
admin.site.register(area_available)
admin.site.register(individualka_photo)
admin.site.register(country_available)
admin.site.register(balance)
admin.site.register(balance_history_plus)
admin.site.register(balance_history_minus)
admin.site.register(comment)

