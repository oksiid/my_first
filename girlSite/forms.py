from django import forms
from django.forms import Textarea ,FileInput, CharField
from .models import *


class individualka_form(forms.ModelForm):
    class Meta:
        model = individualka
        exclude = ['anketa','photo_approved' ,'active_anketa', 'VIP', 'number_of_views', 'comment_have', 'moderation']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control'})
        self.fields['phone_number'].widget.attrs.update({'id': 'phone-mask', 'class':'form-control'})
        self.fields['exit_apartment'].widget.attrs.update({'class': 'custom-control-input', 'id':'customCheck1'})
        self.fields['exit_hotel'].widget.attrs.update({'class': 'custom-control-input', 'id':'customCheck3'})
        self.fields['exit_office'].widget.attrs.update({'class': 'custom-control-input', 'id':'customCheck4'})
        self.fields['exit_sauna'].widget.attrs.update({'class': 'custom-control-input', 'id':'customCheck2'})
        self.fields['her_apartment'].widget.attrs.update({'class': 'custom-control-input', 'id':'customCheck5'})
        self.fields['girl_age'].widget.attrs.update({'id': 'number-mask_1', 'class':'form-control',})
        self.fields['girl_height'].widget.attrs.update({'id': 'number-mask_2', 'class':'form-control',})
        self.fields['girl_weight'].widget.attrs.update({'id': 'number-mask_3', 'class':'form-control',})
        self.fields['girl_chest'].widget.attrs.update({'id': 'number-mask_4', 'class':'form-control',})
        self.fields['tariff_apartment_1'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_apartment_2'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_apartment_night'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_exit_1'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_exit_2'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_exit_night'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_anal'].widget.attrs.update({'class': 'form-control'})
        self.fields['hair_color'].widget.attrs.update({'class': 'form-control'})
        self.fields['intimate_haircut'].widget.attrs.update({'class': 'form-control'})
        self.fields['nationality'].widget.attrs.update({'class': 'form-control'})
        self.fields['essay'].widget.attrs.update({'class': 'form-control'})
        self.fields['usluga_type_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u1'})
        self.fields['usluga_type_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u2'})
        self.fields['usluga_type_3'].widget.attrs.update({'class': 'custom-control-input', 'id':'u3'})
        self.fields['usluga_type_4'].widget.attrs.update({'class': 'custom-control-input', 'id':'u4'})
        self.fields['usluga_laska_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u5'})
        self.fields['usluga_laska_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u6'})
        self.fields['usluga_laska_3'].widget.attrs.update({'class': 'custom-control-input', 'id':'u7'})
        self.fields['usluga_laska_4'].widget.attrs.update({'class': 'custom-control-input', 'id':'u8'})
        self.fields['usluga_laska_5'].widget.attrs.update({'class': 'custom-control-input', 'id':'u9'})
        self.fields['usluga_laska_6'].widget.attrs.update({'class': 'custom-control-input', 'id':'u10'})
        self.fields['usluga_sado_mazo_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u11'})
        self.fields['usluga_sado_mazo_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u12'})
        self.fields['usluga_sado_mazo_3'].widget.attrs.update({'class': 'custom-control-input', 'id':'u13'})
        self.fields['usluga_sado_mazo_4'].widget.attrs.update({'class': 'custom-control-input', 'id':'u14'})
        self.fields['usluga_sado_mazo_5'].widget.attrs.update({'class': 'custom-control-input', 'id':'u15'})
        self.fields['usluga_sado_mazo_6'].widget.attrs.update({'class': 'custom-control-input', 'id':'u16'})
        self.fields['usluga_sado_mazo_7'].widget.attrs.update({'class': 'custom-control-input', 'id':'u17'})
        self.fields['usluga_sado_mazo_8'].widget.attrs.update({'class': 'custom-control-input', 'id':'u18'})
        self.fields['usluga_fisting_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u19'})
        self.fields['usluga_fisting_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u20'})
        self.fields['usluga_zolotoy_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u21'})
        self.fields['usluga_zolotoy_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u22'})
        self.fields['usluga_dopoln_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u23'})
        self.fields['usluga_dopoln_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u24'})
        self.fields['usluga_dopoln_3'].widget.attrs.update({'class': 'custom-control-input', 'id':'u25'})
        self.fields['usluga_finish_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u26'})
        self.fields['usluga_finish_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u27'})
        self.fields['usluga_finish_3'].widget.attrs.update({'class': 'custom-control-input', 'id':'u28'})
        self.fields['usluga_massage_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u29'})
        self.fields['usluga_massage_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u30'})
        self.fields['usluga_massage_3'].widget.attrs.update({'class': 'custom-control-input', 'id':'u31'})
        self.fields['usluga_massage_4'].widget.attrs.update({'class': 'custom-control-input', 'id':'u32'})
        self.fields['usluga_massage_5'].widget.attrs.update({'class': 'custom-control-input', 'id':'u33'})
        self.fields['usluga_massage_6'].widget.attrs.update({'class': 'custom-control-input', 'id':'u34'})
        self.fields['usluga_massage_7'].widget.attrs.update({'class': 'custom-control-input', 'id':'u35'})
        self.fields['usluga_massage_8'].widget.attrs.update({'class': 'custom-control-input', 'id':'u36'})
        self.fields['usluga_strip_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u37'})
        self.fields['usluga_strip_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u38'})
        self.fields['usluga_lesbi_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u39'})
        self.fields['usluga_lesbi_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u40'})
        self.fields['usluga_extrim_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u41'})
        self.fields['usluga_extrim_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u42'})
        self.fields['usluga_copro_1'].widget.attrs.update({'class': 'custom-control-input', 'id':'u43'})
        self.fields['usluga_copro_2'].widget.attrs.update({'class': 'custom-control-input', 'id':'u44'})

class intim_salon_form(forms.ModelForm):
    class Meta:
        model = intim_salon
        exclude = ['anketa','active_anketa', 'number_of_views', 'comment_have', 'moderation']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control'})
        self.fields['address'].widget.attrs.update({'class': 'form-control'})
        self.fields['phone_number_1'].widget.attrs.update({'id': 'phone-mask_1', 'class':'form-control'})
        self.fields['phone_number_2'].widget.attrs.update({'id': 'phone-mask_2', 'class':'form-control'})
        self.fields['phone_number_3'].widget.attrs.update({'id': 'phone-mask_3', 'class':'form-control'})
        self.fields['work24'].widget.attrs.update({'id':'customCheck1','class':'custom-control-input'})
        self.fields['work_time_start'].widget.attrs.update({'class':'form-control'})
        self.fields['work_time_close'].widget.attrs.update({'class':'form-control'})
        self.fields['tariff_1'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_2'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_night'].widget.attrs.update({'class': 'form-control'})
        self.fields['tariff_anal'].widget.attrs.update({'class': 'form-control'})
        self.fields['essay'].widget.attrs.update({'class': 'form-control'})

class individualka_photo_form(forms.Form):
    photos = forms.ImageField(required=False, widget=forms.FileInput(attrs={'multiple': 'multiple', 'class':' custom-file-input', 'id': 'customFile',}))

