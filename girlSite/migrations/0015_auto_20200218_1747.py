# Generated by Django 2.2.4 on 2020-02-18 14:47

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('girlSite', '0014_individualka_number_of_views'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='active_anketa',
            field=models.BooleanField(default=False, verbose_name='Активная анкета'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='area',
            field=models.CharField(max_length=50, verbose_name='Район'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='city',
            field=models.CharField(max_length=50, verbose_name='Город'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='comment_have',
            field=models.BooleanField(default=False, verbose_name='Есть отзывы'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='essay',
            field=models.TextField(verbose_name='Текст о себе'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='exit_apartment',
            field=models.BooleanField(default=False, verbose_name='Выезд аппартаменты'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='exit_hotel',
            field=models.BooleanField(default=False, verbose_name='Выезд отель'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='exit_office',
            field=models.BooleanField(default=False, verbose_name='Выезд офис'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='exit_sauna',
            field=models.BooleanField(default=False, verbose_name='Выезд сауна'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='girl_age',
            field=models.IntegerField(verbose_name='Возраст'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='girl_chest',
            field=models.IntegerField(verbose_name='Грудь'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='girl_height',
            field=models.IntegerField(verbose_name='Рост'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='girl_weight',
            field=models.IntegerField(verbose_name='Вес'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='hair_color',
            field=models.CharField(choices=[('Брюнетка', 'Брюнетка'), ('Шатенка', 'Шатенка'), ('Рыжая', 'Рыжая'), ('Блондинка', 'Блондинка'), ('Русая', 'Русая')], max_length=20, verbose_name='Цвет волос'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='her_apartment',
            field=models.BooleanField(default=False, verbose_name='Личные аппартаменты'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='intimate_haircut',
            field=models.CharField(choices=[('Полная депиляция', 'Полная депиляция'), ('Аккуратная стрижка', 'Аккуратная стрижка'), ('Натуральная', 'Натуральная')], max_length=20, verbose_name='Интимная стрижка'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='metro',
            field=models.CharField(max_length=50, verbose_name='Метро'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='moderation',
            field=models.BooleanField(default=False, verbose_name='Прошло модерацию'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Имя'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='nationality',
            field=models.CharField(choices=[('Абхазки', 'Абхазки'), ('Аварки', 'Аварки'), ('Адыгейки', 'Адыгейки'), ('Азербайджанки', 'Азербайджанки'), ('Азиатки', 'Азиатки'), ('Албанки', 'Албанки'), ('Алтайки', 'Алтайки'), ('Армянки', 'Армянки'), ('Балкарки', 'Балкарки'), ('Башкирки', 'Башкирки'), ('Белоруски', 'Белоруски'), ('Бурятки', 'Бурятки'), ('Вьетнамки', 'Вьетнамки'), ('Гагаузки', 'Гагаузки'), ('Грузинки', 'Грузинки'), ('Еврейки', 'Еврейки'), ('Ингушки', 'Ингушки'), ('Кабардинки', 'Кабардинки'), ('Казашки', 'Казашки'), ('Калмычки', 'Калмычки'), ('Каракалпачки', 'Каракалпачки'), ('Карачаевки', 'Карачаевки'), ('Карелки', 'Карелки'), ('Киргизки', 'Киргизки'), ('Китаянки', 'Китаянки'), ('Коми - пермячки', 'Коми - пермячки'), ('Комячки', 'Комячки'), ('Кореянки', 'Кореянки'), ('Крымчанки', 'Крымчанки'), ('Марийки', 'Марийки'), ('Монголки', 'Монголки'), ('Мордвинки', 'Мордвинки'), ('Мулатки', 'Мулатки'), ('Негритянки', 'Негритянки'), ('Ногайки', 'Ногайки'), ('Осетинки', 'Осетинки'), ('Русские', 'Русские'), ('Таджички', 'Таджички'), ('Татарки', 'Татарки'), ('Тайки', 'Тайки'), ('Тувинки', 'Тувинки'), ('Удмуртки', 'Удмуртки'), ('Узбечки', 'Узбечки'), ('Уйгурки', 'Уйгурки'), ('Украинки', 'Украинки'), ('Хакаски', 'Хакаски'), ('Ханты', 'Ханты'), ('Цыганки', 'Цыганки'), ('Черкески', 'Черкески'), ('Чеченки', 'Чеченки'), ('Чувашки', 'Чувашки'), ('Чукчи', 'Чукчи'), ('Эвенкийки', 'Эвенкийки'), ('Якутки', 'Якутки'), ('Японки', 'Японки'), ('Немки', 'Немки'), ('Кавказки', 'Кавказки'), ('Индианки', 'Индианки')], max_length=20, verbose_name='Национальность'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='number_of_views',
            field=models.IntegerField(default=0, verbose_name='Колличество просмотров'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='phone_number',
            field=models.CharField(max_length=20, verbose_name='Телефон'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='photo_approved',
            field=models.BooleanField(blank=True, default=False, verbose_name='Фото проверенно'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='tariff_anal',
            field=models.IntegerField(verbose_name='Цена анал'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='tariff_apartment_1',
            field=models.IntegerField(verbose_name='Аппартаменты 1 час'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='tariff_apartment_2',
            field=models.IntegerField(verbose_name='Аппартаменты 2 часа'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='tariff_apartment_night',
            field=models.IntegerField(verbose_name='Аппартаменты ночь'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='tariff_exit_1',
            field=models.IntegerField(verbose_name='Выезд 1 час'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='tariff_exit_2',
            field=models.IntegerField(verbose_name='Выезд 2 часа'),
        ),
        migrations.AlterField(
            model_name='individualka',
            name='tariff_exit_night',
            field=models.IntegerField(verbose_name='Выезд Ночь'),
        ),
    ]
