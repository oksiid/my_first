# Generated by Django 2.2.4 on 2020-02-18 08:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('girlSite', '0013_individualka_comment_have'),
    ]

    operations = [
        migrations.AddField(
            model_name='individualka',
            name='number_of_views',
            field=models.IntegerField(default=0),
        ),
    ]
