# Generated by Django 2.2.4 on 2020-02-19 10:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('girlSite', '0017_auto_20200218_2309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='individualka_true',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='comment',
            name='photo_true',
            field=models.BooleanField(default=False),
        ),
    ]
