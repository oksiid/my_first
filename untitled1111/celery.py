import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'untitled1111.settings')
celery_app = Celery('untitled1111')
celery_app.config_from_object('django.conf:settings', namespace='CELERY')
celery_app.autodiscover_tasks()